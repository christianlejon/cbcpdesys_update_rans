import sys
sys.path.append("/scratch/chrlej/data_rans/DoubleBluff")

# read data and write mesh refinement into .mtr file
# In the .node file there is also a boundary marker included for points on (some part of) the boundary.

# Here is a method to create suitable mesh for CFD with high 10e5 Re number coupled with  dispersion. From topology data or from shape data.
# Solve the Eikonal equation on the very coarse mesh to get a distance to boundary measure. Make the computational domain much larger than the domain to be used for the real cfd modeling so that the distance within the small "CFD-domain" will correspond to solid walls/ground.
# Write this information to suitable format (.var or .mtr ? ) for TetGen.
# Create a small domain/mesh with suitable size for cfd in the same coordinate system as the above coarse mesh.
# Refine this smaller cfd mesh with the Eikonal information carrying file with TetGen refinement switch.
# For urban environment one idea is to include the tallest building upstream the release point to a distance 10x the extending height, in order to obtain realistic oncomoing wind/turbulence fields to the release point.

# Read data from .node file and write a .mtr file for mesh refinement
H = 0.04 # Cube dimension
#with open('/scratch/chrlej/data_rans/DoubleBluff/Double_Bluff.2.node', 'r') as reader, open('/scratch/chrlej/data_rans/DoubleBluff/Double_Bluff.2.mtr', 'w') as writer:
with open('/home/chrlej/Backup RANS/DoubleBluff.2.node', 'r') as reader, open(
            '/home/chrlej/Backup RANS/DoubleBluff.2.mtr', 'w') as writer:

    i=1
    # Header
    firstline = reader.readline()
    floats = [float(x) for x in firstline.split()]
    NoP = int(floats[0]) # Number of points in the mesh
    writer.write(str(NoP) + ' ' + '1' + '\n')
    for line in reader:
        if i==NoP + 1: #Passed the last point
            break
        floats = [float(x) for x in line.split()] # # floats[]: 0:node number 1: X -coordinate 2: Y - coordinate 3: Z - coordinate
        # Specify the mesh sizing function
        if floats[2]<2*H and floats[1] < 10*H and abs(floats[3]) < 1.5 * H or floats[2] < H/2: # This is estimated region of required higher resolution from figure 4 in Martinuzzi 2004
            writer.write(str(H / 15) + '\n')
        else:
            writer.write(str(H / 4) + '\n')
        i=i+1

# temps = reader.readlines()
#float(temps[2][8:28]) #X - coordinate
#float(temps[2][30:49]) #Y - coordinate
#float(temps[51][71]) # Z - coordinate