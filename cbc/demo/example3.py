from dolfin import *
import time
import matplotlib.pyplot as plt
import numpy as np
from mshr import *

parameters["std_out_all_processes"] = False
length = 2.20
height = 0.41
r = Rectangle(Point(0, 0), Point(length, height))
r2 = Rectangle(Point(0.5, 0), Point(0.6, 0.1))
g = r - r2
mesh = generate_mesh(g, 200)

rank = MPI.comm_world.Get_rank()
# dijitso clean in terminal to clean JIT

# *** Start problem settings ***
dim3D = True
set_log_level(50)
numPicard = 150  # Total number Picard iterations as a start guess for Newton
numNewton = 40  # Total number Picard iterations as a start guess for Newton
omega = 0.3  # relaxation for Picard iterations
Re = 4.e3
# solver_parameters = {'linear_solver': 'bicgstab', 'preconditioner': 'hypre_euclid'}
solver_parameters = {'linear_solver': 'mumps'}
solver_parameters2 = solver_parameters.copy()
solver_parameters2['maximum_iterations'] = 1  # Inner Newton iterations
solver_parameters2['error_on_nonconvergence'] = False
solver_parameters2 = {"newton_solver": solver_parameters2}
parameters["form_compiler"]["cpp_optimize"] = True
parameters['form_compiler']['cpp_optimize_flags'] = '-O3'

k_start = 0 * 0.1
e_start = 0.1


def fullBoundary(x, on_boundary):
    return on_boundary


# Function spaces
P2 = VectorElement("Lagrange", mesh.ufl_cell(), 1)
P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
TH = P2 * P1
W = FunctionSpace(mesh, TH)
W_ke = FunctionSpace(mesh, P1 * P1)
V = FunctionSpace(mesh, P2)
P = FunctionSpace(mesh, P1)

# Data
nu = Constant((1.0 / Re))

# *** End problem settings ***

# Mesh parameter
h = CellDiameter(mesh)
n = FacetNormal(mesh)

# Works fine. Using Picard iterations for the stabilization terms and Newton for the convection
t0 = time.time()
if rank == 0:
    print(" *** Test time stationary Navier-Stokes equation - Picard and Newton hybrid *** ")

# Test and trial functions
w = TrialFunction(W)
(u, p) = split(w)
(v, q) = TestFunctions(W)

w_ke = TrialFunction(W_ke)
(k, e) = split(w_ke)
(v_k, v_e) = TestFunctions(W_ke)

# Quantities
nu_T = Function(P)


# Boundary conditions
# Subdomains
class InFlow(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.0)


class OutFlow(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 2.2)


class Walls(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and ( near(x[1], 0) or near(x[1], 0.41) or (0.49 < x[0] < 0.61 and x[1] < 0.11))


# Boundary conditions
domain_wall = Walls()
domain_inflow = InFlow()
domain_outldow = OutFlow()
noslip = DirichletBC(W.sub(0), (0, 0), domain_wall)
outflow = DirichletBC(W.sub(1), 0, domain_outldow)
p_in = Expression("1", pi=np.pi, degree=6)
inflow = DirichletBC(W.sub(1), p_in, domain_inflow)
prefC = "x[0] < DOLFIN_EPS && x[1] < DOLFIN_EPS"
pref = DirichletBC(W.sub(1), 0, prefC, "pointwise")
bcs = [noslip, pref]


#lid0 = DirichletBC(W.sub(0), noslipC, top)
#bcs0 = [noslip, lid0, pref]
p_in_0 = Expression("0", degree=1)
inflow0 = DirichletBC(W.sub(1), p_in_0, domain_inflow)
bcs0 = [noslip, pref]


boundaries = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
boundaries.set_all(0)
domain_inflow.mark(boundaries, 1)
ds = ds(subdomain_data = boundaries)

# Residual
def conv(u, u_):
    return grad(u) * u_
    # return div(outer(u, u_))


def res(u, u_, p):
    return grad(p) + grad(u) * u_ - nu * div(sym(grad(u)))


# Stabilization terms SUPG, PSPG, LSIC
def Stab(u, u_, u_const, p, v, q):
    tau_SUPG = ((4.0 * dot(u_const, u_const) / (h ** 2)) + 9 * (4.0 * (nu + nu_T) / h ** 2) ** 2) ** (-0.5)
    tau_PSPG = tau_SUPG
    tau_LSIC = 2 * (nu + nu_T) / 3  # sqrt(dot(u_const, u_const)) * h / 2.
    F_SUPG = inner(tau_SUPG * res(u, u_, p), grad(v) * u_) * dx
    F_PSPG = inner(tau_PSPG * res(u, u_, p), grad(q)) * dx
    F_LSIC = inner(tau_LSIC * div(u), div(v)) * dx
    return F_SUPG + F_LSIC - F_PSPG


def NS(u, u_, p, v, q):
    Fdx = (inner(grad(u) * u_, v) + (nu + nu_T) * inner(sym(grad(u)), (grad(v))) - p * div(v) - div(u) * q) * dx
    Fds = inner(p_in*n, v) * ds(1)
    return Fdx + Fds


# return (inner( div(outer(u, u)), v) + (nu + nu_T) * inner(sym(grad(u)), (grad(v))) - p * div(v) - div(u) * q) * dx

def KE(u_, k, k_, e, e_, v_k, v_e):
    # Project from DG to continuous space
    E = projectDelta_u(E_DG)

    # Coupled model
    F_k = (dot(u_, grad(k)) * v_k + (nu + nu_T / sigma_k) * inner(grad(k),
                                                                  grad(v_k)) + P_k * v_k - e * v_k - D * v_k) * dx

    F_e = (dot(u_, grad(e)) * v_e + (nu + nu_T / sigma_e) * inner(grad(e), grad(v_e)) -
           (C_e1 * P_k - C_e2 * f_2 * e) * (e_ / k_) * v_e + E * v_e) * dx

    return F_k + F_e


# Project a discontinuous to a continuous function
def projectDelta_u(g):
    u_p = TrialFunction(P)
    v_p = TestFunction(P)
    a_p = - inner(grad(u_p), grad(v_p)) * dx + dot(n, grad(u_p)) * v_p * ds
    l_p = g * v_p * dx
    u_p = Function(P)
    solve(a_p == l_p, u_p, solver_parameters=solver_parameters)
    return u_p


# Solve with Newton iterations
if rank == 0:
    print(" *** Picard iterations *** ")

# Start guess (zero) for Picard iterations
w_ = Function(W)
(u_, p_) = w_.split()
ke_ = Function(W_ke)
(k_, e_) = ke_.split()
k_.vector()[:] = k_start
e_.vector()[:] = e_start

# Launder and Sharma model
C_nu = 0.09
sigma_k = 1.
sigma_e = 1.3
C_e1 = 1.44
C_e2 = 1.92
Re_T = k_ * k_ / (nu * (e_ + 1e-6))
f_nu = exp(-3.4 / ((1 + Re_T / 50) * (1 + Re_T / 50)))
f_2 = 1 - 0.3 * exp(-Re_T * Re_T)
D = 2 * nu * dot(grad(sqrt(k_)), grad(sqrt(k_)))
E_DG = 2 * nu * nu_T * dot(div(grad(u_)), div(grad(u_)))
P_k = -2 * nu_T * inner(sym(grad(u_)), grad(u_))

# Define blinear form
F_NS = NS(u, u_, p, v, q) + Stab(u, u_, u_, p, v, q)
a_NS = lhs(F_NS)
l_NS = rhs(F_NS)
# F_ke = KE(u_, u_, k, k_, e, e_, v_k, v_e)
# a_ke = lhs(F_ke)
# l_ke = rhs(F_ke)

# Solution vector
w = Function(W)
ke = Function(W_ke)

w_error = Function(W)
iteration = 0
error = 1
t1 = time.time()
while iteration < numPicard and error > 0.1:

    (u_, p_) = w_.split()
    (k_, e_) = ke_.split()

    nu_T = C_nu * f_nu * k_ * k_ / e_

    solve(a_NS == l_NS, w, bcs, solver_parameters=solver_parameters)
    # solve(a_ke == l_ke, ke, solver_parameters=solver_parameters)

    (u1, p1) = w.split()
    (k1, e1) = ke.split()

    # plt.figure(1)
    # c = plot(k1)
    # plt.colorbar(c)
    # plt.figure(2)
    # c = plot(e1)
    # plt.colorbar(c)
    # plt.show()

    # Error
    w_error.assign(w - w_)
    error = norm(w_error) / norm(w)

    # Update
    w_.assign(omega * w + (1 - omega) * w_)
    # ke_.assign(omega * ke + (1 - omega) * ke_)

    if rank == 0:
        print("Picard iteration: ", iteration, " Error : ", error)
    iteration += 1

if rank == 0:
    print("Solve Picard start guess in time :", time.time() - t1)

# Save last value as a start guess for Newton iterations
w.assign(w_)

# can not use a trial function any more as thous need to be linear in the form
(u, p) = split(w)

# Fully non-linear form (u_=u)
w_.assign(w)
error = 1

# Non-linear form
F = NS(u, u, p, v, q) + Stab(u, u, u, p, v, q)
J = derivative(F, w)
while error > 1.e-9 and iteration < (numNewton + numPicard):

    (u_, p_) = w_.split()

    # Solve non-linear equation with Newton
    # solve(F == 0, w, bcs, J=J, solver_parameters=solver_parameters2)
    delta = Function(W)
    solve(J == -F, delta, bcs0, solver_parameters=solver_parameters)
    w.assign(w_ + 0.7*delta)

    # Error
    w_error.assign(w - w_)
    error = norm(w_error) / norm(w)

    # Update
    w_.assign(w)

    if rank == 0:
        print("Picard (hybrid Newton) iteration: ", iteration, ". Error : ", error)
    iteration += 1

if rank == 0:
    print("Solve time :", time.time() - t0)

# Save velocity and pressure field
(u, p) = w.split()
ufile_u = File("velocity1.pvd")
ufile_u << u
ufile_p = File("pressure1.pvd")
ufile_p << p

# if MPI.
plt.figure(1)
plot(u)
plt.axis('equal')
plt.figure(2)
u_magnitude = sqrt(dot(u, u))
c = plot(u_magnitude)
plt.axis('equal')
plt.colorbar(c)
plt.figure(3)
c = plot(p)
plt.axis('equal')
plt.colorbar(c)
plt.show()
