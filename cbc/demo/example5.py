from dolfin import *
import time
import matplotlib.pyplot as plt
import numpy as np
from dolfin import DirichletBC
from mshr import *

def Max(a, b): return (a+b+abs(a-b))/Constant(2)
def Min(a, b): return (a+b-abs(a-b))/Constant(2)

length = 2.20
height = 0.41
r = Rectangle(Point(0, 0), Point(length, height))
r2 = Rectangle(Point(0.5, 0), Point(0.6, 0.1))
g = r - r2
mesh = generate_mesh(g, 100)

rank = MPI.comm_world.Get_rank()
# dijitso clean in terminal to clean JIT

# *** Start problem settings ***
dim3D = True
set_log_level(50)
numPicard = 1500  # Total number Picard iterations as a start guess for Newton
numNewton = 20  # Total number Picard iterations as a start guess for Newton
omegaP = 0.8  # relaxation for Picard iterations
omegaN = 1  # relaxation for Picard iterations
Re = 4.e2
TOL_P = 5.e-02
# solver_parameters = {'linear_solver': 'bicgstab', 'preconditioner': 'hypre_euclid'}
solver_parameters = {'linear_solver': 'mumps'}
solver_parameters2 = solver_parameters.copy()
solver_parameters2['maximum_iterations'] = 1  # Inner Newton iterations
solver_parameters2['error_on_nonconvergence'] = False
solver_parameters2 = {"newton_solver": solver_parameters2}


k_start = 0.1
e_start = 0.1

def fullBoundary(x, on_boundary):
    return on_boundary


# Function spaces
P2 = VectorElement("Lagrange", mesh.ufl_cell(), 1)
P1 = FiniteElement("Lagrange", mesh.ufl_cell(), 1)
TH = P2 * P1
W = FunctionSpace(mesh, TH)
W_ke = FunctionSpace(mesh, P1 * P1)
V = FunctionSpace(mesh, P2)
P = FunctionSpace(mesh, P1)

# Data
nu = Constant((1.0 / Re))

# *** End problem settings ***

# Mesh parameter
h = CellDiameter(mesh)
n = FacetNormal(mesh)

# Works fine. Using Picard iterations for the stabilization terms and Newton for the convection
t0 = time.time()
if rank == 0:
    print(" *** Test time stationary Navier-Stokes equation - Picard and Newton hybrid *** ")

# Test and trial functions
w = TrialFunction(W)
(u, p) = split(w)
(v, q) = TestFunctions(W)

w_ke = TrialFunction(W_ke)
(k, e) = split(w_ke)
(v_k, v_e) = TestFunctions(W_ke)

# Quantities
nu_T = Function(P)


# Boundary subdomains
class InFlow(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and near(x[0], 0.0)


class WallsNoSlip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[1], 0) or (0.49 < x[0] < 0.61 and x[1] < 0.11))

class WallsSlip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[1], 0.41) or near(x[1], 0) or (0.49 < x[0] < 0.61 and x[1] < 0.11))


# Boundary conditions
# Essential
prefC = "x[0] < DOLFIN_EPS && x[1] < DOLFIN_EPS"
pref = DirichletBC(W.sub(1), 0, prefC, "pointwise")
bcs = [] # [pref]

# Natural
p_in = Expression("1", pi=np.pi, degree=6)
boundaries = MeshFunction("size_t", mesh, mesh.topology().dim()-1)
boundaries.set_all(0)
InFlow().mark(boundaries, 1)
WallsNoSlip().mark(boundaries, 2)
WallsSlip().mark(boundaries, 3)
ds = ds(subdomain_data = boundaries)


# Residual



def NS(u, u_, p, v, q):
    def res(u, u_, p):
        return grad(p) + grad(u) * u_ - nu * div(sym(grad(u)))

    # Stabilization terms SUPG, PSPG, LSIC
    def Stab(u, u_, u_const, p, v, q):
        tau_SUPG = ((4.0 * dot(u_const, u_const) / (h ** 2)) + 9 * (4.0 * (nu + nu_T) / h ** 2) ** 2) ** (-0.5)
        tau_PSPG = tau_SUPG
        tau_LSIC = 2 * (nu + nu_T) / 3  # sqrt(dot(u_const, u_const)) * h / 2.
        F_SUPG = inner(tau_SUPG * res(u, u_, p), grad(v) * u_) * dx
        F_PSPG = inner(tau_PSPG * res(u, u_, p), grad(q)) * dx
        F_LSIC = inner(tau_LSIC * div(u), div(v)) * dx
        return F_SUPG + F_LSIC - F_PSPG

    Fdx = (inner(grad(u) * u_, v) + (nu + nu_T) * inner(sym(grad(u)), (grad(v))) - p * div(v) - div(u) * q) * dx
    Fds = inner(p_in*n, v) * ds(1)
    Fnoslip = (inner( -dot((nu + nu_T)*sym(grad(u)), n) + p*n, v)
             + inner(-dot((nu + nu_T)*sym(grad(v)), n) + q*n, u)
             + 20*(nu + nu_T)/h*inner(u,v))*ds(2)
    Fslip = (inner( -dot((nu + nu_T)*sym(grad(u)), n) + p*n, dot(v, n)*n)
             + inner(-dot((nu + nu_T)*sym(grad(v)), n) + q*n, dot(u, n)*n)
             + 20*(nu + nu_T)/h*inner(dot(u, n), dot(v, n)))*ds(3)

    Fstab = Stab(u, u_, u_, p, v, q)
    return Fdx + Fds + Fslip + Fnoslip + Fstab


# Coupled model
sigma_k = 1.0
sigma_e = 1.3
C_1 = 1.44
C_2 = 1.92
c_bc = 0.01     # value in [0.003,0.01]
Cmy = 0.09
l_0 = 1
kappa = 0.41
y_plus = 11.06

def KE(u_, k, k_, e, e_, v_k, v_e):

    Pk = nu_T*inner(sym(grad(u_)), grad(u_))
    gamma = e_/k_

    # The k-equation
    F_k_Omega = (dot(u_, grad(k)) * v_k + nu_T/sigma_k*inner(grad(k), grad(v_k)) + gamma*k - Pk*v_k) * dx
    k_in = c_bc*dot(u_, u_)
    F_K_Gamma_In = (- dot(n, nu_T/sigma_k*grad(k))*v_k - dot(n, nu_T/sigma_k*grad(v_k))*(k-k_in) + 15/h*(k-k_in)*v_k)*ds(1)
    F_k = F_k_Omega + F_K_Gamma_In

    # The epsilon-equation
    F_e_Omega = (dot(u_, grad(e)) * v_e + nu_T / sigma_e*inner(grad(e), grad(v_e)) + C_2*gamma*e - C_1*Pk*gamma*v_e) * dx
    e_in = Cmy/l_0*k_in**(3/2)
    F_e_in = (- dot(n, nu_T/sigma_e*grad(e))*v_e - dot(n, nu_T/sigma_e*grad(v_e))*(e-e_in) + 15/h*(e-e_in)*v_e)*ds(1)
    u_tau = Cmy**0.25*sqrt(k_)# Max(*sqrt(k_), sqrt(dot(u_))/y_plus)
    F_e_wall = - kappa*u_tau/sigma_e*e*v_e * ds(3)
    F_e = F_e_Omega + F_e_in - F_e_wall # How about the sign for wall?
    return F_k + F_e


# Solve with Newton iterations
if rank == 0:
    print(" *** Picard iterations *** ")

# Start guess (zero) for Picard iterations
w_ = Function(W)
(u_, p_) = w_.split()
ke_ = Function(W_ke)
(k_, e_) = ke_.split()
k_.vector()[:] = k_start
e_.vector()[:] = e_start
nu_T.vector()[:] = Cmy*k_start*k_start/e_start

# Define blinear form
F_NS = NS(u, u_, p, v, q)
a_NS = lhs(F_NS)
l_NS = rhs(F_NS)

# Solution vector
w = Function(W)

w_error = Function(W)
iteration = 0
error = 1
t1 = time.time()
while iteration < numPicard and error > TOL_P:

    (u_, p_) = w_.split()
    (k_, e_) = ke_.split()

    solve(a_NS == l_NS, w, bcs, solver_parameters=solver_parameters)

    # Error
    w_error.assign(w - w_)
    error = norm(w_error) / norm(w)

    # Update
    w_.assign(omegaP * w + (1 - omegaP) * w_)

    if rank == 0:
        print("Picard iteration: ", iteration, " Error : ", error)
    iteration += 1

if rank == 0:
    print("Solve Picard start guess in time :", time.time() - t1)

# Save last value as a start guess for Newton iterations
w.assign(w_)

# can not use a trial function any more as thous need to be linear in the form
(u, p) = split(w)

# Fully non-linear form (u_=u)
w_.assign(w)
error = 1

# Non-linear form
F = NS(u, u, p, v, q)
J = derivative(F, w)
numNewton += iteration
while error > 1.e-9 and iteration < numNewton:

    (u_, p_) = w_.split()

    # Solve non-linear equation with Newton
    # solve(F == 0, w, bcs, J=J, solver_parameters=solver_parameters2)
    delta = Function(W)
    solve(J == -F, delta, bcs, solver_parameters=solver_parameters)
    w.assign(w_ + omegaN*delta)

    # Error
    w_error.assign(w - w_)
    error = norm(w_error) / norm(w)

    # Update
    w_.assign(w)

    if rank == 0:
        print("Picard (hybrid Newton) iteration: ", iteration, ". Error : ", error)
    iteration += 1

if rank == 0:
    print("Solve time :", time.time() - t0)

# Test solve k-e system
iteration = 0
# Define blinear form
(k, e) = TrialFunctions(W_ke)
(v_k, v_e) = TestFunctions(W_ke)
F_KS = KE(u_, k, k_, e, e_, v_k, v_e)
a_KS = lhs(F_KS)
l_KS = rhs(F_KS)
ke = Function(W_ke)
error = 1
ke_error = Function(W_ke)
while iteration < numPicard and error > 1e-6:

    (u_, p_) = w_.split()
    (k_, e_) = ke_.split()

    solve(a_KS == l_KS, ke, solver_parameters=solver_parameters)
    (u1, p1) = w.split()

    # Error
    ke_error.assign(ke - ke_)
    error = norm(ke_error) / norm(ke)

    # Update
    ke_.assign(omegaP * ke + (1 - omegaP) * ke_)

    if rank == 0:
        print("Picard iteration (k-eps): ", iteration, " Error : ", error)
    iteration += 1

# Save velocity and pressure field
(u, p) = w.split()
(k, e) = ke.split()
ufile_u = File("velocity1.pvd")
ufile_u << u
ufile_p = File("pressure1.pvd")
ufile_p << p

# if MPI.
plt.figure(1)
plot(u, title="Velocity")
plt.axis('equal')
plt.figure(2)
u_magnitude = sqrt(dot(u, u))
c = plot(u_magnitude)
plt.axis('equal')
plt.colorbar(c)
plt.figure(3)
c = plot(p, title="Pressure")
plt.axis('equal')
plt.colorbar(c)
plt.figure(4)
c = plot(k, title="k - Turbulent kinetic energy")
plt.colorbar(c)
plt.axis('equal')
plt.figure(5)
c = plot(e, title="epsilon - Dissipation of turbulent kinetic energy")
plt.colorbar(c)
plt.axis('equal')

nu_T = Cmy*k*k/e
plt.figure(6)
c = plot(nu_T, title="V_T - Turbulent viscosity")
plt.colorbar(c)
plt.axis('equal')
plt.show()
