from dolfin import *
import matplotlib.pyplot as plt

mesh = Mesh("dolfin_fine.xml.gz")

V = FunctionSpace(mesh, 'CG', 1)
v = TestFunction(V)
u = TrialFunction(V)
f = Constant(1.0)
y = Function(V)

class DolphinBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and not (near(x[0], 0) or near(x[0], 1.) or near(x[1], 0.) or near(x[1], 1.))

dolphin = DolphinBoundary()

bc = DirichletBC(V, Constant(0), dolphin)

# Initialization problem to get good initial guess for nonlinear problem:
F1 = inner(grad(u), grad(v)) * dx - f * v * dx
solve(lhs(F1) == rhs(F1), y, bc)

# Stabilized Eikonal equation
print("max cell size:", mesh.hmax())
eps = Constant(mesh.hmax() / 25)
F = sqrt(inner(grad(y), grad(y))) * v * dx - f * v * dx + eps * inner(grad(y), grad(v)) * dx
solve(F == 0, y, bc)

print("Max distance:", y.vector().max())

plt.figure(1)
plot(y, rescale=True, title="Eikonal")
plt.show()
