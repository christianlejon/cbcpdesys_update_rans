from dolfin import *
import matplotlib.pyplot as plt
import time
import numpy as np

# list_krylov_solver_preconditioners()
# list_linear_solver_methods()
parameters["form_compiler"]["cpp_optimize"] = True
parameters['form_compiler']['cpp_optimize_flags'] = '-O3'
# parameters["form_compiler"]["quadrature_degree"] = 4
# parameters['form_compiler'].add('eliminate_zeros', True)


set_log_level(20)

rank = MPI.comm_world.Get_rank()

def top(x, on_boundary):
    return x[1] > 1.0 - DOLFIN_EPS

DIM = 3
N = 64
numPicard = 5

# DE: umfpack (direct solver) and ilu (incomplete LU-factorization) does not work in parallel
#solver_parameters = {'linear_solver': 'mumps'}
solver_parameters = {'linear_solver': 'bicgstab', 'preconditioner': 'hypre_euclid'}
solver_parameters2 = {"newton_solver": solver_parameters}

if DIM == 2:
    mesh = UnitSquareMesh(N, N)

    # Function spaces
    P2 = VectorElement("Lagrange", triangle, 1)
    P1 = FiniteElement("Lagrange", triangle, 1)

    f = Constant((0.0, 0.0))

    noslipC = Constant((0, 0))
    lidC = Constant((1, 0))
    prefC = "x[0] < DOLFIN_EPS && x[1] < DOLFIN_EPS"


    def not_top(x, on_boundary):
        return x[0] > (1.0 - DOLFIN_EPS) or x[1] < DOLFIN_EPS or x[0] < DOLFIN_EPS

elif DIM == 3:
    mesh = UnitCubeMesh(N, N, N)

    P2 = VectorElement("Lagrange", tetrahedron, 1)
    P1 = FiniteElement("Lagrange", tetrahedron, 1)

    f = Constant((0.0, 0.0, 0.0))

    noslipC = Constant((0, 0, 0))
    lidC = Constant((1, 0, 0))
    prefC = "x[0] < DOLFIN_EPS && x[1] < DOLFIN_EPS && x[2] < DOLFIN_EPS"


    def not_top(x, on_boundary):
        return x[0] > (1.0 - DOLFIN_EPS) or x[0] < DOLFIN_EPS \
               or x[0] < DOLFIN_EPS \
               or x[2] > (1.0 - DOLFIN_EPS) or x[2] < DOLFIN_EPS
else:
    print("Dim has to be 2 or 3")

TH = P2 * P1
W = FunctionSpace(mesh, TH)
V = FunctionSpace(mesh, P2)
P = FunctionSpace(mesh, P1)

# Data
nu = Constant((1.0 / 4.e2))
dt = 0.01
k = Constant(dt)
T = 3.
omega = 0.7  # relaxation for Picard iterations

# Boundary conditions
noslip = DirichletBC(W.sub(0), noslipC, not_top)
lid = DirichletBC(W.sub(0), lidC, top)
pref = DirichletBC(W.sub(1), 0, prefC, "pointwise")
bcs = [noslip, lid, pref]

# Mesh parameter
h = CellDiameter(mesh)

# Works. Stokes problem (linear)
if False:
    print(" *** Test Stokes equation *** ")


    # Residual
    def Res(u, p):
        return grad(p) - nu * div(sym(grad(u))) - f


    # Test and trial functions
    wS = TrialFunction(W)
    (u, p) = split(wS)
    (v, q) = TestFunctions(W)
    F = (inner(sym(grad(u)), sym(grad(v))) + p * div(v) + div(u) * q - inner(f, v)) * dx
    tau_PSPG = 1. / sqrt(9 * pow(4 * 1 / (h * h), 2))
    F_PSPG = inner(tau_PSPG * Res(u, p), grad(q)) * dx

    a = lhs(F - F_PSPG)
    l = rhs(F - F_PSPG)
    w = Function(W)
    solve(a == l, w, bcs)
    (u, p) = split(w)

    plt.figure(1)
    plot(u, title="Velocity Stokes - TH")
    plt.figure(2)
    plot(p, title="Pressure Stokes - TH")
    plt.show()

# Does not work. Full Newton iterations.
if False:
    print(" *** Test time stationary Navier-Stokes equation - Newton*** ")


    # Residual
    def Res(u, u_, p):
        return grad(u) * u_ + grad(p) - div(nu * sym(grad(u))) - f


    w = Function(W)
    (u, p) = split(w)
    (v, q) = TestFunctions(W)

    # Solve with Newton iterations
    print(" *** Newton iterations *** ")
    F = (inner(grad(u) * u, v) + nu * inner(sym(grad(u)), (grad(v))) - p * div(v) - div(u) * q - inner(f, v)) * dx
    unorm = 1e3  # Needed for convergence. sqrt(dot(u,u))
    tau_SUPG = 1. / sqrt(pow(4 * unorm / h, 2) + 9 * pow(4 * nu / (h * h), 2))
    tau_PSPG = 1. / sqrt(pow(4 * unorm / h, 2) + 9 * pow(4 * nu / (h * h), 2))
    tau_LSIC = unorm * h / 2.
    F_SUPG = inner(tau_SUPG * Res(u, u, p), grad(v) * u) * dx
    F_PSPG = inner(tau_PSPG * Res(u, u, p), grad(q)) * dx
    F_LSIC = inner(tau_LSIC * div(u), div(v)) * dx

    F += F_SUPG - F_PSPG + F_LSIC
    #F += - F_PSPG

    solve(F == 0, w, bcs)

    (u, p) = split(w)
    plt.figure(3)
    plot(u, title="Velocity Navier-Stokes - Newton")
    plt.figure(4)
    plot(p, title="Pressure Navier-Stokes - Newton")

# Works. Picard iterations
if False:
    print(" *** Test time stationary Navier-Stokes equation - Picard *** ")


    # Start guess - Stokes solution
    def Res(u, p):
        return grad(p) - nu * div(sym(grad(u))) - f


    # Test and trial functions
    w = TrialFunction(W)
    (u, p) = split(w)
    (v, q) = TestFunctions(W)
    F_Stokes = (nu * inner(sym(grad(u)), (grad(v))) + p * div(v) + div(u) * q - inner(f, v)) * dx
    tau_PSPG_Stokes = 1. / sqrt(9 * pow(4 * 1 / (h * h), 2))
    F_PSPG_Stokes = inner(tau_PSPG_Stokes * Res(u, p), grad(q)) * dx
    F_Stokes += - F_PSPG_Stokes
    a = lhs(F_Stokes)
    l = rhs(F_Stokes)
    w_ = Function(W)
    solve(a == l, w_, bcs)


    # Residual
    def Res(u, u_, p):
        return grad(u) * u_ + grad(p) - div(nu * sym(grad(u))) - f


    # Solve with Newton iterations
    print(" *** Picard iterations *** ")
    TOL = 1
    (u_, p_) = split(w_)
    iteration = 0
    while TOL > 1.e-9 and iteration < 100:
        F = (inner(grad(u) * u_, v) + nu * inner(sym(grad(u)), (grad(v))) - p * div(v) - div(u) * q - inner(f, v)) * dx
        unorm = sqrt(dot(u_, u_))
        tau_SUPG = 1. / sqrt(pow(4 * unorm / h, 2) + 9 * pow(4 * nu / (h * h), 2))
        tau_PSPG = 1. / sqrt(pow(4 * unorm / h, 2) + 9 * pow(4 * nu / (h * h), 2))
        tau_LSIC = unorm * h / 2.
        F_SUPG = inner(tau_SUPG * Res(u, u_, p), grad(v) * u_) * dx
        F_PSPG = inner(tau_PSPG * Res(u, u_, p), grad(q)) * dx
        F_LSIC = inner(tau_LSIC * div(u), div(v)) * dx

        F += F_SUPG + F_LSIC - F_PSPG
        a = lhs(F)
        l = rhs(F)

        w = Function(W)
        solve(a == l, w, bcs)
        (u1, p1) = split(w)

        # Test tol
        diffU = u1 - u_
        diffP = p1 - p_
        epsU = norm(project(diffU, V), 'L2')
        epsP = norm(project(diffP, P), 'L2')
        relU = norm(project(u1, V), 'L2')
        relP = norm(project(p1, P), 'L2')

        # Update
        w_.assign(omega * w + (1 - omega) * w_)
        (u_, p_) = split(w_)
        iteration += 1
        TOL = epsU / relU + epsP / relP
        print("Picard iteration: ", iteration, " Tol : ", TOL)

    (u, p) = split(w)
    plt.figure(8)
    plot(u, title="Velocity Navier-Stokes - Picard ")
    plt.figure(9)
    plot(p, title="Pressure Navier-Stokes - Picard ")

# Works fine. Using Picard iterations for the stabilization terms and Newton for the convection
if True:
    t0 = time.time()
    if rank == 0:
        print(" *** Test time stationary Navier-Stokes equation - Picard and Newton hybrid *** ")


    # Start guess - Stokes solution
    def Res(u, p):
        return grad(p) - nu * div(sym(grad(u))) - f


    # Test and trial functions
    w = TrialFunction(W)
    (u, p) = split(w)
    (v, q) = TestFunctions(W)
    #F_Stokes = (nu * inner(sym(grad(u)), (grad(v))) + p * div(v) + div(u) * q - inner(f, v)) * dx
    #tau_PSPG_Stokes = 1. / sqrt(9 * pow(4 * 1 / (h * h), 2))
    #F_PSPG_Stokes = inner(tau_PSPG_Stokes * Res(u, p), grad(q)) * dx
    #F_Stokes += - F_PSPG_Stokes
    #a = lhs(F_Stokes)
    #l = rhs(F_Stokes)
    w_ = Function(W)
    # solve(a == l, w_, bcs, solver_parameters=solver_parameters)


    # Residual
    def Res(u, u_, p):
        return grad(u) * u_ + grad(p) - div(nu * sym(grad(u))) - f


    # Solve with Newton iterations
    if rank == 0:
        print(" *** Picard iterations *** ")
    TOL = 1
    (u_, p_) = w_.split()
    iteration = 0
    w = Function(W)

    while iteration < numPicard:
        F = (inner(grad(u) * u_, v) + nu * inner(sym(grad(u)), (grad(v))) - p * div(v) - div(u) * q - inner(f, v)) * dx
        unorm = sqrt(dot(u_, u_))
        tau_SUPG = ((4.0 * dot(u_, u_) / (h ** 2)) + (4.0 * nu / h ** 2) ** 2) ** (-0.5)
        tau_PSPG = ((4.0 * dot(u_, u_) / (h ** 2)) + (4.0 * nu / h ** 2) ** 2) ** (-0.5)
        tau_LSIC = sqrt(dot(u_, u_)) * h / 2.
        F_SUPG = inner(tau_SUPG * Res(u, u_, p), grad(v) * u_) * dx
        F_PSPG = inner(tau_PSPG * Res(u, u_, p), grad(q)) * dx
        F_LSIC = inner(tau_LSIC * div(u), div(v)) * dx

        F += F_SUPG + F_LSIC - F_PSPG
        a = lhs(F)
        l = rhs(F)

        solve(a == l, w, bcs, solver_parameters=solver_parameters)
        (u1, p1) = w.split()

        # Test tol
        # diffU = u1.vector().get_local() - u_.vector().get_local()
        # diffP = p1.vector().get_local() - p_.vector().get_local()
        # epsU = np.linalg.norm(diffU, ord=np.Inf)
        # epsP = np.linalg.norm(diffP, ord=np.Inf)
        # relU = np.linalg.norm(u1.vector().get_local(), ord=np.Inf)
        # relP = np.linalg.norm(p1.vector().get_local(), ord=np.Inf)


        #epsU = norm(diffU, 'L2')
        #epsP = norm(interpolate(diffP, P), 'L2')
        #relU = norm(interpolate(u1, V), 'L2')
        #relP = norm(interpolate(p1, P), 'L2')

        # Update
        w_.assign(omega * w + (1 - omega) * w_)
        (u_, p_) = w_.split()
        # TOL = (2. / 3.) * epsU / relU + (1. / 3.) * epsP / relP
        iteration += 1

        if rank == 0:
            print("Picard iteration: ", iteration, " Tol : ", TOL)
        # print("iter : ", iteration)
        # MPI.barrier(0)

    # print("test : ", rank)

    (u, p) = split(w)
    # Mix Newton and Picard
    TOL = 1
    # while TOL > 1.e-9 and iteration < 100:
    F = (inner(grad(u) * u, v) + nu * inner(sym(grad(u)), (grad(v))) - p * div(v) - div(u) * q - inner(f, v)) * dx
        # unorm = sqrt(dot(u_, u_))
    tau_SUPG = ((4.0*dot(u, u)/(h**2)) + (4.0*nu/h**2)**2)**(-0.5)
    tau_PSPG = ((4.0*dot(u, u)/(h**2)) + (4.0*nu/h**2)**2)**(-0.5)
    tau_LSIC = sqrt(dot(u, u)) * h / 2.
    F_SUPG = inner(tau_SUPG * Res(u, u, p), grad(v) * u) * dx
    F_PSPG = inner(tau_PSPG * Res(u, u, p), grad(q)) * dx
    F_LSIC = inner(tau_LSIC * div(u), div(v)) * dx

    F += F_SUPG + F_LSIC - F_PSPG

    J = derivative(F, w)

    solve(F == 0, w, bcs, J=J, solver_parameters = solver_parameters2)
    (u, p) = w.split()

    #    # Test tol
    #    diffU = u1 - u_
    #    diffP = p1 - p_
    #    epsU = norm(project(diffU, V), 'L2')
    #    epsP = norm(project(diffP, P), 'L2')
    #    relU = norm(project(u1, V), 'L2')
    #    relP = norm(project(p1, P), 'L2')

        # Update
    #    w_.assign(omega * w + (1 - omega) * w_)
    #    (u_, p_) = split(w_)
    #    iteration += 1
    #    TOL = epsU / relU + epsP / relP
    #    print("Newton iteration: ", iteration, " Tol : ", TOL)
    if rank == 0:
        print("Solve time :", time.time() - t0)

    # plt.figure(8)
    # plot(u, title="Velocity Navier-Stokes - Picard")
    # plt.figure(9)
    # plot(p, title="Pressure Navier-Stokes - Picard")
    # Compute magnitude of displacement
    # u_magnitude = sqrt(dot(u, u))
    # plt.figure(10)
    # c = plot(u_magnitude, title="Velocity magnitude Navier-Stokes - Picard")
    # plt.colorbar(c)

ufile_pvd1 = File("velocity.pvd")
ufile_pvd1 << u
#ufile_pvd2 = File("magnitude.pvd")
#ufile_pvd2 << u_magnitude

# ufile_pvd << u1
# plt.show()

# plt.show()
