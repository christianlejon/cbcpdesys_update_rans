# Here I will apply a RANS model and RANS solver to the double bluff wind tunnel formulation as presented in "Vortex shedding from two surface-mounted cubes in tandem" by Robert J. Martinuzzi and Brian Havel in International Journal of Heat and Fluid Flow 25 (2004) 364-372.
# Christian Lejon  
# Starting from demo channel.py

from cbc.cfd import icns                    # Navier-Stokes solvers
from cbc.cfd import ransmodels              # RANS models
from cbc.cfd.icns import solver_parameters  # parameters for NS
from cbc.cfd.ransmodels import solver_parameters as rans_parameters # parameters for RANS model
import collections as c
from cbc.cfd.problems.NSProblem import *
from dolfin import *
from mshr import *
import sys
sys.path.append("/scratch/chrlej/PycharmProjects/cbc/cfd/data/DoubleBluff/")
# import meshio

from numpy import arctan, array
import matplotlib.pyplot as plt

class DoubleBluff(NSProblem):
    def __init__(self, parameters):
        NSProblem.__init__(self, parameters=parameters)
        self.mesh = self.gen_mesh()
        self.boundaries = self.create_boundaries()
        self.q0 = laminar_velocity

    def gen_mesh(self):
#        # Create mesh from built-in functions of mshr
        #self.S = problem_parameters['S']
        #self.mesh_res = problem_parameters['mesh_res']
        self.H = H = problem_parameters['H']   # Cube dimensions
        #Spacing = self.S * H  # Spacing between cubes. Variable
        self.LCh = LCh = 16 * H  #  This will correspond to mesh generated in Rhino-TetGen
#        h_inv = (LCh / H) * self.mesh_res  # Mesh resolution in FEniCS measure
#        channel = Box(Point(0, 0, -6 * H), Point(LCh, 6 * H, 6 * H))
#        cube1 = Box(Point(2 * H, 0, -H / 2), Point(3 * H, H, H / 2))
#        cube2 = Box(Point(3 * H + Spacing, 0, -H / 2), Point(4 * H + Spacing, H, H / 2))
#        domain = channel - cube1 - cube2
#        m = generate_mesh(domain, h_inv)

        m = mesh("/scratch/chrlej/PycharmProjects/cbc/cfd/data/DoubleBluff/DoubleBluff_lowres.xml")
        return m

    def create_boundaries(self): #TODO: what is the ke BC's on inflow and outflow? Why does ke system diverge?
        self.mf = MeshFunction('size_t', self.mesh, 2)  # Facets
        # self.mf = MeshFunction_CL('size_t', self.mesh, 1)
        # self.mf = MeshFunction_CL('size_t', self.mesh, 1, boundary_indicators = 1) # Create a MeshFunction that has boundary_indicators as well.
        self.mf.set_all(0)

        walls = FlowSubDomain(lambda x, on_boundary: ((near(x[1], 0.) or
                                                       near(x[1], self.H) or
                                                       near(x[1], 3 * self.H) or
                                                       near(x[2], -3 * self.H) or
                                                       near(x[2], 3 * self.H) or
                                                       near(x[0], 2 * self.H) or
                                                       near(x[0], 3 * self.H) or
                                                       near(x[0], 5 * self.H) or
                                                       near(x[0], 6 * self.H) or
                                                       near(x[2], - self.H / 2.) or
                                                       near(x[2],   self.H / 2.)) and
                                                      on_boundary),
                              bc_type='Wall',
                              mf=self.mf)

        # symmetry = FlowSubDomain(lambda x, on_boundary: near(x[1], 0.) and on_boundary,
        # bc_type = 'Symmetry',
        # mf = self.mf)
        if self.prm['periodic']:
            self.pressure_gradient = Constant((2. / self.prm['Re'], 0., 0.))

            def periodic_map(x, y):
                y[0] = x[0] - self.LCh
                y[1] = x[1]
                y[2] = x[2]
            p = periodic_map
            p.L = self.LCh

            periodic = FlowSubDomain(lambda x, on_boundary: near(x[0], self.LCh) and on_boundary,
                                     bc_type='Periodic',
                                     mf=self.mf,
                                     periodic_map=p)
            bcs = [walls, periodic]

        else:

            self.pressure_gradient = Constant((0, 0, 0))

            # Note that VelocityInlet should only be used for Steady problem
            inlet    = FlowSubDomain(lambda x, on_boundary: near(x[0], 0.) and on_boundary,
            bc_type = 'VelocityInlet', func = laminar_velocity, mf = self.mf)
            outlet   = FlowSubDomain(lambda x, on_boundary: near(x[0], self.LCh) and on_boundary,
            bc_type = 'Outlet', func = {'p': Constant(0.0)}, mf = self.mf)
            #inlet = FlowSubDomain(lambda x, on_boundary: near(x[0], 0.) and on_boundary,
            #                      bc_type='ConstantPressure',
            #                      func={'p': Constant(2. / self.prm['Re'] * self.L)},
            #                      mf=self.mf)
            #
            #outlet = FlowSubDomain(lambda x, on_boundary: near(x[0], self.L) and on_boundary,
            #                       bc_type='ConstantPressure',
            #                       func={'p': Constant(0.0)},
            #                       mf=self.mf)

            bcs = [walls, inlet, outlet]

        return bcs

    def body_force(self):
        return self.pressure_gradient

    def __info__(self):
        return 'Double bluff experiment'


if __name__ == '__main__':
    ## Set up problem ##
    # The double bluff/vortex shedding problem
    laminar_velocity = Initdict(u=(("8.8", "0.", "0")), p=("0")) # Laminar velocity profiles that can be used in Expressions for initializing the solution or prescribing a VelocityInlet.
    #laminar_velocity = Initdict(u=(("8.8", "0", "0")),
    #                            p=("0"))
    #laminar_velocity = {'u': Constant((8.8, 0, 0)),
    #               'u0': Constant(0),
    #               'u1': Constant(0),
    #               'u2': Constant(0),
    #                'p': Constant(0)}

    #laminar_velocity = Initdict(u=(("8.8", "0", "0")), u0=("8.8"), u1=("0"), u2=("0"), p=("0"))

    ## Double bluff parameters ##
    problem_parameters['periodic'] = False
    #problem_parameters['mesh_res'] = 2. # Use only when mesh generated in mshr. The characteristic mesh resolution "elements per obstacle". Somewhere between 10-30 is appropriate says Jan Burman the king.
    problem_parameters['S'] = 2.  # Spacing between cubes in cube dimension (Spacing=S*H) (variable in experimental setup)
    problem_parameters['H'] = H = 0.04 # Cube height.
    problem_parameters['periodic'] = False
    problem_parameters['time_integration'] = 'Steady'
    #problem_parameters['Re'] = Re = 2.2e4 # "kinematic' Reynolds number for the double bluff
    problem_parameters['Re'] = Re = 8
    problem_parameters['plot_velocity'] = False
    problem_parameters['plot_all'] = False
    # problem_parameters['make_pseudo_time_plot'] = False
    problem_parameters['save_solution'] = True
    problem_parameters['Filename_save_solution'] = 'Steady_Re8_lowres'
    problem_parameters['max_err'] = 1e-2 # CL 1e-7 default
    problem_parameters['max_iter'] = 100
    #problem_parameters['turbulence_model'] = 'Chien'  # CL - use this together with ransmodels.LowReynolds_Coupled. JonesLaunder and Chien are other alternatives
    problem_parameters['turbulence_model'] = 'NSonly'
    #problem_parameters['viscosity'] = 1.48e-5 # This yields slightly different results when calculating the Reynolds number in the double bluff experiment. But should be used in the real case I suppose. Air kinematic viscosity  at 15 °C  https://www.engineersedge.com/physics/viscosity_of_air_dynamic_and_kinematic_14483.htm
    problem_parameters['viscosity'] = float(laminar_velocity['u'][0])*H/Re

    ## Choose problem ##

    problem = DoubleBluff(problem_parameters)
    # Update the dictionary used for initialization.
    problem.q0.update(dict(
        k=0.01,
        e=0.01,
        ke=('0.01',)*2), #, #TODO. Is this a correct implementation ? /CL
        #v2=('0.001'), # I guess v2 is not for k-epsilon model
        #f=('1.'), # I guess f is not for k-epsilon model
        #R=('0.001',)*3, #  Seems not to apply to k-epsilon model
        #Rij=('0.0',)*9, #  Seems not to apply to k-epsilon model
        #Fij=('0.0',)*9 #  Seems not to apply to k-epsilon model
        )


    ## Set up Navier-Stokes solver ##

    #solver_parameters['degree']['u'] = 2 #Use Taylor-Hood for non stabilized forms for convergence.
    solver_parameters['omega'].default_factory = lambda : 0.5
    solver_parameters['max_iter'] = 1
    solver_parameters['plot_velocity'] = False
    solver_parameters['plot_pressure'] = False
    solver_parameters['plot_pressure_inner'] = False # CL - For debugging
    solver_parameters['print_up_sol_info'] = False
    solver_parameters['pdesubsystem'] = c.defaultdict(lambda: 1) # CL - choose variational form. 666 - Stabilzed SUPG, PSPG, ICXX by danelf. For Steady_Coupled: 1 - Simplest possible steady form.  2 - Use explicit convection and preassemble the coefficient matrix for Picard iterations. For Newton it's still nonlinear and needs reassembling. 3 - Pseudo-Transient form. For Newton the transient term is zero though. 4 - Simple steady form to illustrate that one can preassemble parts of the system. 5 - Coupled form for use with Reynolds stress model.
    solver_parameters['max_err'] = 1e1 # CL

    # solver_parameters['space'] = {'u': VectorFunctionSpace} Alternative way of writing(?)

    solver_parameters = recursive_update(solver_parameters, {
        'space': {'u': VectorFunctionSpace},
        'element': {'u': VectorElement},
        'degree': {'u': 1},
        'time_integration': 'Steady',
        #'linear_solver': {'u': 'minres', 'p': 'cg', 'up': 'minres'}, #TODO: minres seems not to solve the ['up'] nor ['u', 'p'] system correctly
        #'precond': {'u': 'amg', 'p': 'amg', 'up': 'amg'},
        'linear_solver': {'u': 'bicgstab', 'p': 'gmres', 'velocity_update': 'bicgstab'}, # From aneurysm.py
        'precond': {'u': 'jacobi', 'p': 'hypre_amg', 'up': 'hypre_amg', 'velocity_update': 'jacobi'}, # From aneurysm.py
        # 'monitor_convergence': {'u': True},
    })

    #Choose solver for Navier-Stokes equation
    NS_solver = icns.NSCoupled(problem, solver_parameters)
    #NS_solver = icns.NSSegregated(problem, solver_parameters)
    #NS_solver = icns.NSFullySegregated(problem, solver_parameters)

    ## Set up turbulence model ##
    #rans_parameters['omega'].default_factory = lambda: 0.7
    rans_parameters['omega'].default_factory = lambda : 0.05 # CL - playing around to get convergence
    rans_parameters['max_iter'] = 2 # CL - playing around to get convergence
    rans_parameters['plot_ke'] = False
    rans_parameters['plot_nut'] = False
    rans_parameters['print_ke_sol_info'] = False  # CL - print solution information at each iteration
    #rans_parameters['pdesubsystem'] = c.defaultdict(lambda: 1) # CL - "Override" default form
    rans_parameters['max_err'] = 1e-1  # CL

    #Choose solver for turbulence problem
    #Turb_solver = ransmodels.LowReynolds_Coupled(problem, rans_parameters,
    #                                           model=problem_parameters['turbulence_model'])
    #Turb_solver = ransmodels.LowReynolds_Segregated(problem, rans_parameters,
    #                                    model=problem_parameters['turbulence_model'])  # CL - segregated (?) solver for k-e Launder-sharma?
    #Turb_solver = ransmodels.StandardKE_Coupled(problem, rans_parameters,
    #                                           model=problem_parameters['turbulence_model'])


    ## Solve the problem ##

    t0 = time()
    if MPI.rank(MPI.comm_world) == 0:
        print('Starting problem.solve')
    problem.solve()
    t1 = time()
    if MPI.rank(MPI.comm_world) == 0:
        print('time to solve DoubleBluff =  ', t1-t0)

    ## Plot ##
    #plt.figure(1)
    #plot(NS_solver.u_, title="u_")
    #plt.show()

    ## Write to file for visualization in eg Paraview ##
    #file_S = File("u_vortex_shedding.pvd")
    #file_S << NS_solver.u_

    # dump_result(problem, solver, t1, 0) TODO: dump interesting results ...
