__author__ = "Mikael Mortensen <Mikael.Mortensen@ffi.no>, Christian Lejon <christian.lejon@foi.se>"
__date__ = "20190415"
__copyright__ = "Copyright (C) 2010 " + __author__
__license__  = "GNU GPL version 3 or any later version"
"""
Switching to segregated approach for both NS and turbulence model.
Goal is to get convergence for high Re problems. Based on lejon_turbulent_channel /CL
"""
"""
Turbulent channel flow.

For turbulent flows it is common to characterize the flow using Re_tau, 
a Reynolds number based on friction velocity u_tau=sqrt(nu*du/dy)_wall.
        
        Re_tau = u_tau/nu
        
The constant pressure gradient is computed by integrating the NS-equations
across the height of the channel

\int_{-1}^{1}(dp/dx) dV = -\int_{-1}^{1} nu*d^2/dy^2u dV 
2*dp/dx = - nu*(du/dy_top- du/dy_bottom) = - (-u_tau**2 - u_tau**2)
dp/dx = u_tau**2
"""

#from dolfin import *
import matplotlib.pyplot as plt
#import pdb
#import sys
#sys.path.append('/home/hakgra/workspaec/fenics/cbc.pdesys-1.0.0_dev') # Can also be added to the PYTHONPATH. ? In terminal python3: export PYTHONPATH="$PWD/cbc.pdesys-1.0.0_dev" Maybe in script?
#from cbc.pdesys import *


# import the channel mesh and parameters from the laminar problem case
from cbc.cfd.problems.channel import *
from cbc.cfd import icns                    # Navier-Stokes solvers
from cbc.cfd import ransmodels              # RANS models
from cbc.cfd.icns import solver_parameters  # parameters for NS
from cbc.cfd.ransmodels import solver_parameters as rans_parameters # parameters for RANS model
import collections as c
# set_log_active(True)

from time import time
from scipy.interpolate import InterpolatedUnivariateSpline as ius
from pylab import zeros, linspace
import pickle

# Postprocessing
'''def tospline(problem): 
    """Store the results in a dictionary of splines and save to file.
    The channel solution is very often used to initialize other problems
    that have a VelocityInlet."""
    NS_solver = problem.pdesystems['Navier-Stokes']
    Turb_solver = problem.pdesystems['Turbulence model']
    f = open('../data/channel_' + problem.prm['turbulence_model'] + '_' + 
                str(problem.prm['Re_tau']) + '.ius','w')
    N = 1000
    xy = zeros(N)
    xy = zeros((N, 2))
    xy[:, 1] = linspace(0., 1., 1000) # y-direction
    spl = {}
    for s in (NS_solver, Turb_solver):            
        for name in s.names:
            vals = zeros((N, s.V[name].num_sub_spaces() or 1))
            for i in range(N):
                getattr(s, name + '_').eval(vals[i, :], xy[i, :])
            spl[name] = []
            # Create spline for all components
            # Scalar has zero subspaces, hence the or
            for i in range(s.V[name].num_sub_spaces() or 1): 
                spl[name].append(ius(xy[:, 1], vals[:, i]))
        
    cPickle.dump(spl, f)
    f.close()
    return spl
'''
if __name__=='__main__':
    # Set up turbulent channel problem
    # Turbulent channel only works with periodic boundaries
    problem_parameters['L'] = 1.
    problem_parameters['periodic'] = True
    problem_parameters['time_integration'] = 'Steady'
    #problem_parameters['Re_tau'] = Re_tau= 395.
    problem_parameters['Re_tau'] = Re_tau= 8
    problem_parameters['utau'] = utau = 0.05
    problem_parameters['plot_velocity'] = False
    problem_parameters['plot_all'] = False
    problem_parameters['save_solution'] = True
    problem_parameters['Filename_save_solution'] = 'Steady_Re8e-211_Nx10Ny100'
    problem_parameters['Ny'] = 100
    problem_parameters['Nx'] = 10
    problem_parameters['max_err'] = 1e-2 # CL 1e-7 default
    problem = channel(problem_parameters)
    problem.prm['viscosity'] = utau/Re_tau
    #problem.prm['dt'] = 0.05 # CL - Override default pseudo-time step for eg. Steady_Couple_3 form.
    problem.pressure_gradient = Constant((utau**2, 0.)) # turbulent pressure gradient
    # Update the dictionary used for initialization.
    problem.q0.update(dict(
        k=0.01,
        e=0.01,
        v2=('0.001'),
        f=('1.'), 
        R=('0.001',)*3,
        Rij=('0.0',)*4, 
        Fij=('0.0',)*4))

    ## Set up Navier-Stokes solver ##
    # solver_parameters['element']['u'] = VectorElement
    solver_parameters['degree']['u'] = 1
    #solver_parameters['degree']['u'] = 2 # CL - playing around to get convergence
    #solver_parameters['omega'].default_factory = lambda : 0.8
    solver_parameters['omega'].default_factory = lambda : 0.1 # CL - playing around to get convergence
    solver_parameters['max_iter'] = 100 # CL - playing around to get convergence
    solver_parameters['plot_velocity'] = False
    solver_parameters['plot_pressure'] = False
    solver_parameters['plot_pressure_inner'] = False # CL - For debugging
    solver_parameters['print_up_sol_info'] = False
    solver_parameters['pdesubsystem'] = c.defaultdict(lambda: 1) # CL - choose variational form. For Steady_Coupled: 1 - Simplest possible steady form.  2 - Use explicit convection and preassemble the coefficient matrix for Picard iterations. For Newton it's still nonlinear and needs reassembling. 3 - Pseudo-Transient form. For Newton the transient term is zero though. 4 - Simple steady form to illustrate that one can preassemble parts of the system. 5 - Coupled form for use with Reynolds stress model.
    solver_parameters['max_err'] = 1e1 # CL

    #Choose solver
    #NS_solver = icns.NSCoupled(problem, solver_parameters)
    NS_solver = icns.NSSegregated(problem, solver_parameters)
    #NS_solver = icns.NSFullySegregated(problem, solver_parameters)

    ## Set up turbulence model ##
    # CL - Testing LowReynoldsSegregated, LowReynoldsCoupled, StandardKE, StandardKE_Coupled etc. to find out the difference and see which one I shall use. LowReynoldsSegregated is my guess...

    #problem_parameters['turbulence_model'] = 'LaunderSharma' # CL - use this together with ransmodels.LowReynolds_Coupled. JonesLaunder and Chien are other alternatives.
    #problem_parameters['turbulence_model'] = 'Chien'
    problem_parameters['turbulence_model'] = 'NSonly_np2' #This sets the path for "save_solution" switch as well
    rans_parameters['omega'].default_factory = lambda: 0.7
    #rans_parameters['omega'].default_factory = lambda : 0.05 # CL - playing around to get convergence
    rans_parameters['max_iter'] = 2000 # CL - playing around to get convergence
    rans_parameters['plot_ke'] = False
    rans_parameters['plot_nut'] = False
    # problem_parameters['plot_ke'] = True # CL - plot ke at each iteration
    rans_parameters['print_ke_sol_info'] = False  # CL - print solution information at each iteration
    #rans_parameters['pdesubsystem'] = c.defaultdict(lambda: 1) # CL - "Override" default form
    rans_parameters['max_err'] = 1e1  # CL


    #Turb_solver = ransmodels.StandardKE_Coupled(problem, rans_parameters,
    #                                            model=problem_parameters['turbulence_model']) # For StandardKE_coupled 'turbulence_model' does not need to be specified'. Perhaps add different wall laws here ? Kuzmin etc,
    #Turb_solver = ransmodels.StandardKE_Kuzmin(problem, rans_parameters,
    #                                            model=problem_parameters['turbulence_model'])
    #Turb_solver = ransmodels.LowReynolds_Coupled(problem, rans_parameters,
    #                                            model=problem_parameters['turbulence_model'])
    #Turb_solver = ransmodels.StandardKE(problem, rans_parameters,
    #                                            model=problem_parameters['turbulence_model'])
    #Turb_solver = ransmodels.LowReynolds_Segregated(problem, rans_parameters,
    #                                    model=problem_parameters[
    #                                        'turbulence_model'])  # CL - segregated (?) solver for k-e Launder-sharma?
    #Turb_solver.e_d = Constant(0.1) # It think this e_d is defined default deeper in the code and it makes no difference here //CL

    ## solve the problem ##

    t0 = time()
    #problem_parameters['max_iter'] = 100
    problem_parameters['max_iter'] = 2000 # CL - playing around
    problem.solve()
    t1 = time()
    if MPI.rank(MPI.comm_world) == 0:
        print('time = ', t1-t0)

    # print(list_timings())
    #print('norm of k', norm(Turb_solver.k_.vector()))
    #print('norm of e', norm(Turb_solver.e_.vector()))
    #plt.figure(1)
    #plt.subplot(131)
    #plot(NS_solver.u_, title="u_")
    #plt.subplot(132)
    #plot(Turb_solver.e_, title="e_")
    #plt.subplot(133)
    #plot(Turb_solver.k_, title="k_")
    #plt.show()

    # Write to file for visualization in eg Paraview
    #file_S = File("u_lejon_turbulent_channel_Re200.pvd")
    #file_S << NS_solver.u_