__author__ = "Mikael Mortensen <Mikael.Mortensen@ffi.no>"
__date__ = "2010-08-30"
__copyright__ = "Copyright (C) 2010 " + __author__
__license__  = "GNU GPL version 3 or any later version"
"""
Base class for all Navier-Stokes problems.
"""
from cbc.pdesys.Problem import *

problem_parameters = copy.deepcopy(default_problem_parameters)
problem_parameters = recursive_update(problem_parameters, {
    'Re': 50.,
    'viscosity': None,
    'cfl': 1.0,
    'save_solution': False,
    'plot_velocity': False,
    'plot_pressure': False,    
    'turbulence_model': 'Laminar',
    'make_pseudo_time_plot': False,  # CL
    'make_time_plot': False,  # CL
    'plot_all': False #CL
})

class NSProblem(Problem):
    
    def __init__(self, mesh=None, parameters=None):
        Problem.__init__(self, mesh=mesh, parameters=parameters)
        try:
            self.output_location = os.environ['CBCRANS']
        except KeyError:
            info_red('Set the environment variable CBCRANS to control the location of stored results')            
            self.output_location = os.getcwd()
        
    def body_force(self):
        return Constant((0.,)*self.mesh.geometry().dim())
        
    def timestep(self, U=1.):
        h = self.mesh.hmin()
        dt = self.prm['cfl']*h**2/(U*(self.prm['viscosity'] + h*U))
        n  = int(self.prm['T'] / dt + 1.0)
        return self.prm['T']/n

    def prepare(self):
        if self.prm['make_pseudo_time_plot']: # CL - Steady systems
            pass

        if self.prm['make_time_plot']:
            pass # Transient system

    def update(self):
        """Called at end of timestep for transient simulations or at the end of
        iterations for steady simulations. """
        if self.prm['save_solution']:
            if self.prm['time_integration'] == 'Steady':
                i = self.total_number_iters
            else:
                i = self.tstep
            # result_path = os.path.join(self.output_location, 'cbc',
            #                           'rans', 'results')
            #result_path = self.result_path = os.path.join(self.output_location, self.pdesystems['Turbulence model'].__class__.__name__)
            #result_path = self.result_path = os.path.join(self.output_location, self.__class__.__name__, self.NS_solver.__class__.__name__, self.pdesystems['Turbulence model'].__class__.__name__)
            result_path = self.result_path = os.path.join(self.output_location, self.__class__.__name__,
                                                          self.NS_solver.__class__.__name__,
                                                          problem_parameters['turbulence_model'])

            #for pt in (self.__class__.__name__,
            #           self.NS_solver.__class__.__name__,  # Alternative CL: self.pdesystems['Navier-Stokes'].__class__.__name__
            #           #self.prm['Model']): #CL  -  deleted and replaced
            #           self.pdesystems['Turbulence model'].__class__.__name__):
            #    result_path = self.result_path = os.path.join(result_path, pt)
            #    if not os.path.exists(result_path):
            #        os.mkdir(result_path)
            if MPI.rank(MPI.comm_world) == 0:
                if not os.path.exists(result_path):
                    if MPI.rank(MPI.comm_world) == 0:
                        os.makedirs(result_path)
                        #os.mkdir(result_path)

                if self.prm['time_integration'] == 'Steady':
                    # Create files for saving
                    if not hasattr(self, 'vtk_file_u'):
                        self.vtk_file_u = File(result_path + '/' + self.prm['Filename_save_solution'] + '_u_' + '.pvd')
                        self.vtk_file_p = File(result_path + '/' + self.prm['Filename_save_solution'] + '_p_' + '.pvd')
                        self.vtk_file_e = File(result_path + '/' + self.prm['Filename_save_solution'] + '_e_' + '.pvd')
                        self.vtk_file_k = File(result_path + '/' + self.prm['Filename_save_solution'] + '_k_' + '.pvd')
                        self.vtk_file_nut = File(result_path + '/' + self.prm['Filename_save_solution'] + '_nut_' + '.pvd')
                    # Save files
                    self.vtk_file_u << (self.NS_solver.u_, self.total_number_iters)
                    self.vtk_file_p << (self.NS_solver.p_, self.total_number_iters)
                    if 'Turbulence model' in self.pdesystems:
                        self.vtk_file_k << (self.pdesystems['Turbulence model'].k_, self.total_number_iters)
                        self.vtk_file_e << (self.pdesystems['Turbulence model'].e_, self.total_number_iters)
                        self.vtk_file_nut << (self.pdesystems['Turbulence model'].nut_, self.total_number_iters)
                else:
                    if (i - 1) % self.prm["save_solution"] == 0:
                        # Create files for saving
                        for name in self.system_names:
                            if not name in self.resultfile:
                                self.resultfile[name] = File(os.path.join(result_path, name + ".pvd"))
                            self.resultfile[name] << self.q_[name]

        if self.prm['plot_velocity']:
            if isinstance(self.NS_solver.u_, ufl.tensors.ListTensor):
                V = VectorFunctionSpace(self.mesh, 'CG',
                                        self.NS_solver.prm['degree']['u0'])
                u_ = project(self.NS_solver.u_, V)
                if hasattr(self, 'u_plot'):
                    self.u_plot.vector()[:] = u_.vector()[:]
                else:
                    self.u_plot = u_
            else:
                self.u_plot = self.NS_solver.u_
            plot(self.u_plot, title='Velocity', rescale=True)

        if self.prm['plot_pressure']: plot(self.NS_solver.p_, title='Pressure',
                                       rescale=True)
        if self.prm['plot_all']:
            import matplotlib.pyplot as plt
            plt.figure(1)
            plt.clf()
            plt.subplot(141)
            plot(self.NS_solver.u_, title="u_")
            plt.subplot(142)
            plot(self.pdesystems['Turbulence model'].e_, title="e_")
            plt.subplot(143)
            plot(self.pdesystems['Turbulence model'].k_, title="k_")
            plt.subplot(144)
            plot(self.NS_solver.p_, title='Pressure')
            plt.waitforbuttonpress()
            #plt.show()




